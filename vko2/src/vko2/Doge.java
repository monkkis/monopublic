package vko2;

//luokat nimet��n alkamaan isolla kirjaimella
//tiedoston nimi on sama kuin luokan nimi
public class Doge {
	
	//luokan j�senmuuttuja eli attribuutti (private n�kyy vain luokan sis�ll�)
	//n�m� yleens� private
	private String name="Doggo";
	
	//olioilla on rakentaja (muodostaja), joka on metodi ja saman niminen kuin luokka
	//eli kun olio luodaan voidaan rakentajalla tehd� jotain samalla
	//huomaa eka iso kirjain
	public Doge(String n) {
		name=n;
	}
	
	public void speak(String a) {
		System.out.println("Hei vaan " + a);
	}
	
	//huomaa, metodin nimess� eka kirjain pieni
	//metodit eli j�senfunktiot eli funktiot sit� vastoin m��ritell��n julkisiksi
	//lukijametodi (accessor) palauttaa olion j�senmuuttujan arvon
	public String getName() {
		return name;
	}
	
	//muuttajametodi (modifier) muuttaa olion attribuutin arvoa
	public void setName(String n) {
		name=n;
	}
	
}

//metodin sis�ll� m��ritellyt muuttujat n�kyv�t vain metodin sis�puolella!
//eli aaltosulkeiden sis�ll� m��ritellyt muuttujat

//Kuormittaminen (overloading): Samasta metodista voidaan tehd� eri versioita
//esim. int methodX(int) ja int method(float), k��nt�j� osaa valita oikean k�ytettyjen parametrien perusteella
//pelk�n paluuarvon muuttaminen ei riit�

//rekursiiviset metodit: funktio kutsuu itse itse��n, esim. Fibonacci