package LoopitjaTaulukot;

public class Minion {
	
	int number=0;
	
	public Minion (int n) {
		number =n;
	}
	
	public void sayHi() {
		System.out.println("Hey, I'm minion number " + number);
	}
	
	public void sayBye() {
		System.out.println("Bye bye banana");
	}
	
	public int getNumber() {return number;}
	
	@Override //kaikilla olioilla on metodi toString, mutta sit� voi muuttaa
	public String toString() {
		return "Hey, I'm minion number " + number + " and I survived";
	}
	
	@Override //suoritetaan, kun olio poistetaan (java poistaa olion, jos sit� ei en�� k�ytet� miss��n
	public void finalize() {
		System.out.println("Oh no, I'm getting killed! - " + number);
	}

}
