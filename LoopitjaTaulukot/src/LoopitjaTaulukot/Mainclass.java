package LoopitjaTaulukot;

import java.util.ArrayList;

public class Mainclass {
	public enum Days {
			MONDAY, TUESDAY, WEDNESDAY, THURSDAY
		}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Days day =null;
		day=Days.THURSDAY;
		
		switch (day) {
		case MONDAY:
			System.out.println("It's monday");
			break;
		case TUESDAY:
			System.out.println("It's tuesday");
			break;
		case WEDNESDAY:
			System.out.println("It's wednesday");
			break;
		default:
			System.out.println("It's weekend");
			break;
		}
		
		int [] numbers= {1,2,3,4,5,6,7,8,9,10}; //tavallinen taulukko
		
		ArrayList<Minion> minions = new ArrayList<Minion>();
		
		for (int i=0; i<numbers.length;i++) {
			Minion temp=new Minion(numbers[i]); //luodaan minionit
			minions.add(temp); //talletetaan ne arraylistaan
		}
		
		//k�yd��n arraylista l�pi
		for (Minion mini : minions) {
			mini.sayHi();
		}
		
		System.out.println("ArrayList size then");
		System.out.println(minions.size());
		
		int index=0;
		do {
			minions.remove(index);
		} while (minions.get(index).getNumber() !=4);
		
		System.out.println("ArrayList size now");
		System.out.println(minions.size());
		
		while (minions.size() > 2) {
			Minion temp2=minions.get(0);
			temp2.sayBye();
			minions.remove(temp2);
		}
		
		//tulostetaan Minion-olioiden tiedot
		System.out.println("ArrayList toString");
		System.out.println(minions.toString());
		System.gc(); //kutsutaan puolimanuaalisesti Garbage Collectoria: finalize suoritetaan nyt
	}

}
