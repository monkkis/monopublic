package viikko2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Mainclass {

	public static void main(String[] args) {
		
		String input1="";
		String input2="";
		
		//jos ei rakentajaa oliossa, niin kaksi seuraavaa rivi�
		//Doge dog1=new Doge();
		//dog1.setName("Erkki");
		
		//Doge dog1=new Doge("Erkki"); //rakentaja oliossa
		//Doge dog2=new Doge("Pekka"); //rakentaja oliossa
		
		//olio on kuin mik� tahansa muuttuja, jonka sisuksiin p��see k�siksi "."-operaattorilla
		//System.out.println("Nimeni on: "+dog1.getName());
		
		//System.out.println("Sano jotain: ");
		System.out.println("Anna koiran nimi: ");
		Scanner scanner=new Scanner(System.in);
		input1=scanner.nextLine();
		Doge dog1=new Doge(input1);
		//dog1.speak(input1);
		
		System.out.println("Anna lausahdus: ");
		//Scanner scanner=new Scanner(System.in);
		input1=scanner.nextLine();
		dog1.speak(input1);
		
		/*System.out.println("Nimeni on: "+dog2.getName());
		System.out.println("Sano jotain uudestaan: ");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {
			input2=br.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("VIRHE!");
		}
		dog2.speak(input2);*/
		scanner.close();
		

	}

}
