package viikko2;

import java.util.Scanner;

//luokat nimet��n alkamaan isolla kirjaimella
//tiedoston nimi on sama kuin luokan nimi
public class Doge {
	
	//luokan j�senmuuttuja eli attribuutti (private n�kyy vain luokan sis�ll�)
	//n�m� yleens� private
	//private String name="Doge";
	private String name;
	private String says;
	private boolean tulostettu=false;
	
	//olioilla on rakentaja (muodostaja), joka on metodi ja saman niminen kuin luokka
	//eli kun olio luodaan voidaan rakentajalla tehd� jotain samalla
	//huomaa eka iso kirjain
	public Doge(String n) {
		boolean tyhjanimi=true;
		
		//jos merkkijonossa joku muu merkki kuin v�lily�nti, niin tyhjanimi=false
		for (int i = 0; i < n.length(); i++) {
		    if (n.charAt(i)!=' '){
		    	tyhjanimi=false;
		    }
		}
		// javan loogiset operaattorit: || (tai),  && (ja)
		if (tyhjanimi || n.length()==0) {
			name="Doge";
		}
		else {
			name=n;
		}
		says="Much wow!";
		System.out.println("Hei, nimeni on " + name+"!");
	}
	
	public void speak(String a) {
		boolean tyhja;
		//System.out.println("Hei vaan " + a);
		//toinen tapa tarkistaa onko annettu merkkijono tyhj�
		
		//https://www.tutorialspoint.com/java/util/scanner_nextboolean.htm
		
		if ((a.trim()).isEmpty()) {
			System.out.println(name+": "+says);
			tyhja=true;
		}
		else {
			System.out.println(name+": "+a);
			tyhja=false;
		}
		
		if (tyhja) {
			System.out.println("Anna uusi lausahdus: ");
			Scanner scanner=new Scanner(System.in);
			a=scanner.nextLine();
			speak(a);
			scanner.close();
		}
		if (Boolean.parseBoolean(a)) {
			System.out.println("Annoit boolean arvon");
			tulostettu=true;
		}
		
		try{
		    int number = Integer.parseInt(a);
		    System.out.println("Annoit kokonaisluvun");
		    tulostettu=true;
		}catch (NumberFormatException ex) {
		    //handle exception here
		}
		
		if ((!tyhja)&&(!tulostettu)) {
			
			//Tutkitaan scannerilla merkkijono a. Merkkijono voi olla lause, kuten "Laika on true koira ja numero 1 avaruudessa."
			Scanner scanner = new Scanner(a);

		    // find the next boolean token and print it
		    // loop for the whole scanner
		    while (scanner.hasNext()) {

		    	// if the next is boolean, print found and the boolean
		        if (scanner.hasNextBoolean()) {
		           System.out.println("Lauseessa oli boolean arvo: " + scanner.nextBoolean());
		        }
		        
		        else if (scanner.hasNextInt()) {
		            System.out.println("Lauseessa oli kokonaisluku: " + scanner.nextInt());
		         }

		        // if a boolean is not found, print "Not Found" and the token
		        //System.out.println("Not Found :" + scanner.next());
		        else {
		        	String dummy = scanner.next(); //t�ll� menn��n seuraavaan sanaan loopissa
		        }
		    }

		     // close the scanner
		     scanner.close();
		}
		
	}
	
	//huomaa, metodin nimess� eka kirjain pieni
	//metodit eli j�senfunktiot eli funktiot sit� vastoin m��ritell��n julkisiksi
	//lukijametodi (accessor) palauttaa olion j�senmuuttujan arvon
	public String getName() {
		return name;
	}
	
	//muuttajametodi (modifier) muuttaa olion attribuutin arvoa
	public void setName(String n) {
		name=n;
	}
	
}

//metodin sis�ll� m��ritellyt muuttujat n�kyv�t vain metodin sis�puolella!
//eli aaltosulkeiden sis�ll� m��ritellyt muuttujat

//Kuormittaminen (overloading): Samasta metodista voidaan tehd� eri versioita
//esim. int methodX(int) ja int method(float), k��nt�j� osaa valita oikean k�ytettyjen parametrien perusteella
//pelk�n paluuarvon muuttaminen ei riit�

//rekursiiviset metodit: funktio kutsuu itse itse��n, esim. Fibonacci